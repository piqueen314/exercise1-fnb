﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPhoneHW1
{
  public class SmartPhone
  {
    //fields
    public string make;
    public string model;
    public int screenSize;
    public int screenResolution;
    public int cameraMegapixels;
    public int dimensions;
    public int batteryCapacity;
    public string processor;
    public int storageSpace;
    //constructor initialize smart phone properies
    public SmartPhone()
    {
      make = "Samsung";
      model = "Galaxy 5";
      screenSize = 6;
      screenResolution = 1080;
      cameraMegapixels = 7;
      dimensions = 10;
      batteryCapacity = 5;
      processor = "ARM";
      storageSpace = 16;


    }
  }

  class testPhone
  {
    static void Main(string[] args)
    {
      SmartPhone phone1 = new SmartPhone();
      Console.WriteLine("Here are some phone properties\n");
      Console.WriteLine("Battery Capacity ="+phone1.batteryCapacity+" hours");
      Console.WriteLine("make="+phone1.make);
      Console.WriteLine("model="+phone1.model);
      Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine(phone1.ToString());
      //Keep the console window open in debug mode.
      Console.WriteLine("Press any key to exit.");

      Console.ReadKey();
    }
  }
}